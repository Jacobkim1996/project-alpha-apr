from tasks.models import Task
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect


from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView


# Create your views here.


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/new.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.assignee = self.request.user
        item.save()
        return redirect("list_projects")


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"

    def get_queryset(self):
        querystring = self.request.GET.get("q")
        if querystring is None:
            querystring = ""
        print(self.queryset)
        return Task.objects.filter(assignee=self.request.user)


class TaskUpdateView(UpdateView):
    model = Task
    fields = ["is_completed"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.assignee = self.request.user
        item.save()
        return redirect("show_my_tasks")
